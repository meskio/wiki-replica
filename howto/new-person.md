---
title: How to get a new Tor System Administrator on board
---

Note that this documentation needs work, as it overlaps with normal
user management procedures, see [issue 40129](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40129).

# Glossary

 * TSA: Tor System Administrators
 * TPA: Tor Project Admins, synonymous with TSA, preferably used to
   disambiguate with [the other TSAs](https://en.wikipedia.org/wiki/TSA)
 * TPI: Tor Project Inc. the company that employs Tor staff
 * TPO: TorProject.Org, machines officially managed by TSA
 * TPN? torproject.net, machines in DNS but not officially managed by TSA
 * a sysadmin can also be a service admin, and both can be paid work

# Orienteering

 * [sysadmin wiki](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/)
 * [service list](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service)
 * [machines list](https://db.torproject.org/machines.cgi)
 * key machines:
   * Puppet: `pauli`
   * [jump host](/doc/ssh-jump-host) and "general shell server": `perdulce`
   * Nagios: `hetzner-hel1-01.torproject.org`
   * LDAP: `alberti`
   * IRC idling host: `chives`
 * key services:
   * [Grafana](howto/grafana): <https://grafana.torproject.org>, password on `admin/tor-passwords.git`
   * [git](howto/git): <https://gitweb.torproject.org/>, or `git@git-rw.torproject.org` over SSH
   * [GitLab](howto/gitlab): <https://gitlab.torproject.org/> - issue tracking and project management
   * [RT](howto/rt): <https://rt.torproject.org/> - not really used by TPA yet
   * spec: <https://spec.torproject.org/> - for a series of permalinks
     to use everywhere, including especially `bugs.tpo/NNN`
 * key mailing lists:
   * <tor-project@lists.torproject.org> - Open list where anyone is welcome to watch but posting is moderated. Please favor using this when you can.
   * <tor-internal@lists.torproject.org> - If something truly can't include the wider community then this is the spot.
   * <tor-team@lists.torproject.org> - Exact same as tor-internal@ except that the list will accept email from non-members. If you need a cc when emailing a non-tor person then this is the place.
   * <tor-employees@lists.torproject.org> - TPI staff mailing list
   * <tor-meeting@lists.torproject.org> - for public meetings
   * <torproject-admin@torproject.org> - TPA-specific "mailing list"
     (not a mailing list but an alias)
 * IRC channels:
   * `#tor-project` - general torproject channel
   * `#tpo-admin` - channel for TPA specific stuff
   * `#tor-internal` - channel for private discussions, need secret
     password and being added to the `@tor-tpomember` with GroupServ,
     part of the `tor-internal@lists.tpo` welcome email)
   * `#tor-bots` - where a lot of bots live
   * `#tor-nagios` ... except the nagios bot, which lives here
   * `#tor-meeting` - where some meetings are held
   * `#tor-meeting2` - fallback for the above
 * TPI stuff: see employee handbook from HR

# Important documentation

 1. [Getting to know LDAP](howto/ldap#getting-to-know-ldap)
 2. [SSH jump host configuration](doc/ssh-jump-host)
 3. [How to edit this wiki](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/documentation#editing-the-wiki-through-git), make sure you have a local copy of the
    documentation!
 4. [Puppet primer: adding yourself to the allow list](howto/puppet#adding-an-ip-address-to-the-global-allow-list)
 5. [New machine creation](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/new-machine)
 6. [Updating status.tpo](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/status#creating-new-issues)

# More advanced documentation

 1. [Account creation procedures](howto/create-a-new-user)
 2. Password manager procedures (undocumented, see
    `ssh://git@git-rw.torproject.org/admin/tor-passwords.git` for now)
 3. [Adding](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/static-component#adding-a-new-component) and [removing](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/static-component#removing-a-component) websites in the [static mirror
    system](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/static-component)
 4. [Editing DNS](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/dns#editing-a-zone)
 5. [TLS certificate operations](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/tls#how-to-get-an-x509-certificate-for-a-domain-with-lets-encrypt)
 6. [Puppet code linting](howto/puppet#validating-puppet-code) and the entire [Puppet operations manual](howto/puppet)
 7. [Backup restore procedures](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/backup)
 8. [Documentation design](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service/documentation#design)
 9. [Ganeti operations manual](https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/ganeti)

# Accounts to create

This section is specifically targeted at *existing* sysadmins, which
should follow this checklist to create the necessary accounts on all
core services. More services might be required if the new person is
part of other service teams, see the [service list](service) for the
exhaustive list.

The first few steps are part of the TPI onboarding process and might
already have been performed.

Here's a checklist that should be copy-pasted in a ticket:

 1. [ ] mailing lists (`tor-internal@` and others, see list above)
 2. [ ] [about/people](https://torproject.org/about/people) web page ([source code](https://gitlab.torproject.org/tpo/web/tpo/-/tree/master/content/about/people))
 3. [ ] GitLab `-admin` account
 4. [ ] GitLab `tpo/tpa` group membership
 5. [ ] [New LDAP account](howto/create-a-new-user)
 6. [ ] [puppet](howto/puppet) git repository access (how?)
 7. [ ] TPA password manager access (`admin/tor-passwords.git` in gitolite)
 8. [ ] [Nagios](howto/nagios) access, contact should be created in
    `ssh://git@git-rw.torproject.org/admin/tor-nagios`, password in
    `/etc/icinga/htpasswd.users` directly on the server
 10. [ ] Sunet cloud access (e.g. `Message-ID: <87bm1gb5wk.fsf@nordberg.se>`)

Extra services we are not directly responsible for, but that new TPA
staff will typically *have* to deal with:

 1. [ ] [BBB](howto/conference) access
 2. [ ] [Nextcloud](service/nextcloud) (undocumented: add to TPA group at least)
 3. [ ] [RT](howto/rt#new-rt-admin)
 4. [ ] [blog](service/blog)
 5. [ ] [btcpayserver](service/BTCpayserver)
 6. [ ] [gitolite admin](howto/git)
 7. [ ] [schleuder](service/schleuder)
