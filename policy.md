# Policies

The policies below document major architectural decisions taken in the
history of the team. This process is similar to the [Network Team Meta
Policy][]. More details of the process is available in the first
policy, [tpa-rfc-1-policy](policy/tpa-rfc-1-policy).

[Network Team Meta Policy]: https://gitlab.torproject.org/legacy/trac/-/wikis/org/teams/NetworkTeam/MetaPolicy

To add a new policy, create the page using the [template](policy/template)
and add it to the above list.

## Draft

 * [TPA-RFC-3: tools](policy/tpa-rfc-3-tools)

## Proposed

No policy is currently `proposed`.

## Standard

 * [TPA-RFC-1: RFC process](policy/tpa-rfc-1-policy)
 * [TPA-RFC-2: Support](policy/tpa-rfc-2-support)
 * [TPA-RFC-5: GitLab migration](policy/tpa-rfc-5-gitlab)
 * [TPA-RFC-6: Naming Convention](policy/tpa-rfc-6-naming-convention)
 * [TPA-RFC-7: root access](policy/tpa-rfc-7-root)
 * [TPA-RFC-8: GitLab CI libvirt exception](policy/tpa-rfc-8-gitlab-ci-libvirt)
 * [TPA-RFC-10: Jenkins retirement](policy/tpa-rfc-10-jenkins-retirement)

## Rejected

No policy has been `rejected` so far.

## Obsolete

 * [TPA-RFC-4: Prometheus disk space change](policy/tpa-rfc-4-prometheus-disk)
 * [TPA-RFC-9: "proposed" status and small process changes](tpa-rfc-9-proposed-process)
