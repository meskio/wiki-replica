We keep minutes of our meetings here.

[[_TOC_]]

<!-- a full list can be found with: -->

<!-- ls -d meeting/*.md | sed 's/.md$//;s/\(.*\)/ * [\1](\1)/' -->

<!-- but this is really just manually  curated now -->

<!-- and yes, those are HTML comments in a markdown document, -->
<!-- amazingly, this does what you would expect. or maybe not. -->

# 2021

 * [2021-05-03](meeting/2021-05-03) (report only)
 * [2021-04-07](meeting/2021-04-07) (report only)
 * [2021-03-02](meeting/2021-03-02)
 * [2021-02-02](meeting/2021-02-02)
 * [2021-01-26](meeting/2021-01-26)
 * [2021-01-19](meeting/2021-01-19)

# 2020

 * [2020-12-02](meeting/2020-12-02)
 * [2020-11-18](meeting/2020-11-18)
 * [2020-07-01](meeting/2020-07-01)
 * [2020-06-10](meeting/2020-06-10)
 * [2020-05-11](meeting/2020-05-11)
 * [2020-04-14](meeting/2020-04-14)
 * [2020-03-09](meeting/2020-03-09)
 * [2020-02-03](meeting/2020-02-03)
 * [2020-01-13](meeting/2020-01-13)

# 2019

 * [2019-11-25](meeting/2019-11-25)
 * [2019-11-04](meeting/2019-11-04)
 * [2019-10-07](meeting/2019-10-07)
 * [2019-09-09](meeting/2019-09-09)
 * [2019-07-01](meeting/2019-07-01)
 * [2019-06-03](meeting/2019-06-03)
 * [2019-05-06](meeting/2019-05-06)
 * [2019-04-08](meeting/2019-04-08)
 * [2019-03-04](meeting/2019-03-04)

# Templates and scripts

 * [monthly-report](meeting/monthly-report)
 * [template](meeting/template)
